<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;

class PertanyaanController extends Controller
{

    public function index() {
        // $pertanyaan = DB::table('pertanyaan')->get();
        $pertanyaan = Pertanyaan::All();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"],
        //     "profil_id" => 1
        // ]);
        
        // Pertanyaan::create([
    	// 	'judul' => $request->judul,
        //     'isi' => $request->isi,
        //     "profil_id" => 1
        // ]);
        
        // ini tidak ada profil_id nya
        // Pertanyaan::create($request->all()); // Linked to Model: Pertanyaan.php

        $isi = $request->all();
        $isi["profil_id"] = 1;
        Pertanyaan::create($isi);
        return redirect('/pertanyaan');
    }
    
    public function show($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request["judul"],
        //         'isi' => $request["isi"]
        //     ]);
        Pertanyaan::where('id', $id)
        ->update([
            'judul' => $request["judul"],
            'isi' => $request["isi"]
        ]);
        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan');
    }

}